package ai.turbochain.ipex.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jack
 * @Title: 
 * @Description:
 * @date 2020/4/289:45
 */
@Data
@AllArgsConstructor
public class CoinDTO {
    private String name;
    private String unit;
}
